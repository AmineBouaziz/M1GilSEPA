DROP DATABASE IF EXISTS SEPA;

CREATE TABLE SEPA (
	transaction VARCHAR(255),
	montant INT,
	num VARCHAR(100),
	identifiant VARCHAR(100),
	date DATETIME
);