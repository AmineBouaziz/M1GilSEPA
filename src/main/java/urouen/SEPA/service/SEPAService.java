package urouen.SEPA.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import urouen.SEPA.dao.ISEPADao;
import urouen.SEPA.model.SEPA;

import java.util.List;

@Service
public class SEPAService implements ISEPAService {

    @Autowired
    private ISEPADao sepaDao;

    public List<SEPA> getResume() {
        List<SEPA> sepaResumeViewList = sepaDao.findAll();

        return sepaResumeViewList;
    }
}
