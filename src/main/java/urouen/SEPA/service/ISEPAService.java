package urouen.SEPA.service;

import urouen.SEPA.model.SEPA;

import java.util.List;

public interface ISEPAService {

    List<SEPA> getResume();
}
