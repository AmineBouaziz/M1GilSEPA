package urouen.SEPA.dao;

import org.hibernate.*;
import org.springframework.stereotype.Repository;
import urouen.SEPA.model.SEPA;

import java.util.List;

@Repository
public class SEPADao implements ISEPADao{


    /* Method to  READ all the employees */
    public List findAll( ){
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        List sepas = null;
        try{
            tx = session.beginTransaction();
            sepas = (List<SEPA>) session.createQuery("FROM urouen.SEPA.model.SEPA").list();
            System.out.println(sepas);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return sepas;
    }
}
