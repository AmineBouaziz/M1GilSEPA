package urouen.SEPA.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import urouen.SEPA.model.Document;
import urouen.SEPA.service.ISEPAService;

@Path("/")
public class SepaController {

    private Document document = new Document();

    public SepaController() {

    }
    
    @Autowired
    private ISEPAService sepaService;

    /**
     * GET method stats
     */
    @GET
    @Path("/stats")
    @Produces("application/xml")
    public Document.Stats getStats() throws JAXBException {
        return document.getStats();
    }

    /**
     * GET method resume
     */
    @GET
    @Path("/resume")
    @Produces("application/xml")
    public Document.Resume getResume() throws JAXBException {
        return document.getResume();
    }

    /**
     * GET method trx
     */
    @GET
    @Path("/trx/n")
    @Consumes({"text/xml", "application/xml", "application/*+xml", "application/json" })
    @Produces("application/xml")
    public Document.Trx getTrx(@PathParam("n") String n) throws JAXBException {
        document.setNameTransaction(n);
        return document.getTrx();
    }

    /**
     * POST method depot
     */
    @POST
    @Path("/depot")
    @Consumes({"text/xml", "application/xml", "application/*+xml"})
    @Produces("application/xml")
    public void postDepot(@RequestBody Document.DrctDbtTxInf transaction) throws JAXBException {
        document.setDepot(transaction);
    }
}
