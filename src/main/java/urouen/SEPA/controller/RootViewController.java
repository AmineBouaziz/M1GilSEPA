package urouen.SEPA.controller;


import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import urouen.SEPA.rest.Client;

public class RootViewController implements Initializable {

    //@FXML private MenuItem miTestConnection;
    @FXML private MenuItem miQuitter;
    @FXML private TabPane tabPane;
   	@FXML private TextArea stats;
   	@FXML private TextArea resume;
   	@FXML private TextArea trx;
   	@FXML private TextArea depot;
   	@FXML private TextField idTrxField;
    
    private Client ctrl;
    
	private Service service;
    

    private static final QName qname = new QName("", "");
    private static final String url = "http://localhost:8080/SEPA";

    public RootViewController(Client Client) {
        this.ctrl = Client;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        miQuitter.setOnAction( event -> ctrl.getPrimaryStage().close() );
    }
    
    @FXML
	private void pressOk() {
		switch (tabPane.getSelectionModel().getSelectedItem().getText()) {
			case "stats" :
				getResultStats();
				break;
			case "resume" :
				getResultResume();
				break;
			case "trx" :
				getResultTrx();
				break;
			case "depot" :
				getResultDepot();
				break;
		}
	}
    
    public String request(String urlParam, String httpMethod, String body) {
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + urlParam);
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("Content-Type", Arrays.asList(new String[] {"application/xml"}));
        headers.put("Accept", Arrays.asList(new String[] {"application/xml"}));
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
 
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, httpMethod);
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer;
        String strResult = "";
		try {
			transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return strResult;
    }
    
    private void getResultDepot() {
		depot.setText(request("/depot","POST", ""));
		
	}

	private void getResultTrx() {
		trx.setText(request("/trx/" + idTrxField.getText().replaceAll(" ", "%20"),"GET", ""));
		
	}

	private void getResultResume() {
		resume.setText(request("/resume","GET", ""));
		
	}

	private void getResultStats() {
		stats.setText(request("/stats","GET", ""));
	}

    
    
   
}
