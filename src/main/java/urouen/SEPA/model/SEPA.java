package urouen.SEPA.model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Entity
@Table(name = "SEPA")
@XmlRootElement(name = "SEPA")
public class SEPA {

	@Column(name = "transaction")
	String transaction;

	@Column(name = "montant")
	int montant;

	// numéro d'enregistrement de la transaction
	@Column(name = "num")
	String num;

	//contenu de la balise <PmtId>
	@Column(name = "identifiant")
	String identifiant;

	//date de la transaction
	@Column(name = "date")
	Date date;
	
	public String getTransaction() { 
		return transaction; 
	}
	
	@XmlElement
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	
	public int getMontant() { 
		return montant; 
	}
	
	@XmlElement
	public void setMontant (int montant) {
		this. montant = montant;
	}
	
	public SEPA(String transaction, int montant) {
		this.transaction = transaction;
		this.montant = montant;
	}
	
	public SEPA() {
		
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}