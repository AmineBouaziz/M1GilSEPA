//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.03.21 à 01:49:07 PM CET 
//


package urouen.SEPA.model;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.univ.sepa package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.univ.sepa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link CmpsdIdt }
     * 
     */
    public CmpsdIdt createCmpsdIdt() {
        return new CmpsdIdt();
    }

    /**
     * Create an instance of {@link CmpsdIdt.PrvtId }
     * 
     */
    public CmpsdIdt.PrvtId createCmpsdIdtPrvtId() {
        return new CmpsdIdt.PrvtId();
    }

    /**
     * Create an instance of {@link CmpsdIdt.PrvtId.Othr }
     * 
     */
    public CmpsdIdt.PrvtId.Othr createCmpsdIdtPrvtIdOthr() {
        return new CmpsdIdt.PrvtId.Othr();
    }

    /**
     * Create an instance of {@link CmpsdAgt }
     * 
     */
    public CmpsdAgt createCmpsdAgt() {
        return new CmpsdAgt();
    }

    /**
     * Create an instance of {@link Document.DrctDbtTxInf }
     * 
     */
    public Document.DrctDbtTxInf createDocumentDrctDbtTxInf() {
        return new Document.DrctDbtTxInf();
    }

    /**
     * Create an instance of {@link CmpsdNmd }
     * 
     */
    public CmpsdNmd createCmpsdNmd() {
        return new CmpsdNmd();
    }

    /**
     * Create an instance of {@link CmpsdTx }
     * 
     */
    public CmpsdTx createCmpsdTx() {
        return new CmpsdTx();
    }

    /**
     * Create an instance of {@link Amount.InstdAmt }
     * 
     */
    public Amount.InstdAmt createAmountInstdAmt() {
        return new Amount.InstdAmt();
    }

    /**
     * Create an instance of {@link CmpsdIdt.PrvtId.Othr.SchmeNm }
     * 
     */
    public CmpsdIdt.PrvtId.Othr.SchmeNm createCmpsdIdtPrvtIdOthrSchmeNm() {
        return new CmpsdIdt.PrvtId.Othr.SchmeNm();
    }

    /**
     * Create an instance of {@link CmpsdAgt.Othr }
     * 
     */
    public CmpsdAgt.Othr createCmpsdAgtOthr() {
        return new CmpsdAgt.Othr();
    }

}
