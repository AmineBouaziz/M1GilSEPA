//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.03.21 à 01:49:07 PM CET 
//


package urouen.SEPA.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.print.Doc;
import javax.xml.bind.annotation.*;


/**
* <p>Classe Java pour anonymous complex type.
* 
* <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
* 
* <pre>
* &lt;complexType>
*   &lt;complexContent>
*     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*       &lt;sequence>
*         &lt;element name="DrctDbtTxInf" maxOccurs="10">
*           &lt;complexType>
*             &lt;complexContent>
*               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                 &lt;sequence>
*                   &lt;element name="PmtInf" type="{http://univ.fr/sepa}Max35Text"/>
*                   &lt;element name="InstdAmt" type="{http://univ.fr/sepa}Amount"/>
*                   &lt;element name="DrctDbtTx" type="{http://univ.fr/sepa}CmpsdTx"/>
*                   &lt;element name="DbtrAgt" type="{http://univ.fr/sepa}CmpsdAgt"/>
*                   &lt;element name="Dbtr" type="{http://univ.fr/sepa}CmpsdNmd"/>
*                   &lt;element name="DbtrAcct" type="{http://univ.fr/sepa}CmpsdIdt"/>
*                   &lt;element name="RmtInf" type="{http://www.w3.org/2001/XMLSchema}string"/>
*                 &lt;/sequence>
*               &lt;/restriction>
*             &lt;/complexContent>
*           &lt;/complexType>
*         &lt;/element>
*       &lt;/sequence>
*     &lt;/restriction>
*   &lt;/complexContent>
* &lt;/complexType>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 "drctDbtTxInf",
 "stats",
 "resume",
 "trx",
 "nameTransaction"
})
@XmlRootElement(name = "Document")
public class Document {

 @XmlElement(name = "DrctDbtTxInf", required = true)
 protected List<Document.DrctDbtTxInf> drctDbtTxInf;
 protected Document.Stats stats;
 protected Document.Resume resume;
 protected Document.Trx trx;

 protected String nameTransaction;

 public Document() {
     stats = new Stats(this);
     resume = new Resume(this);
     trx = new Trx(this);
 }

 /**
  * Gets the value of the drctDbtTxInf property.
  * 
  * <p>
  * This accessor method returns a reference to the live list,
  * not a snapshot. Therefore any modification you make to the
  * returned list will be present inside the JAXB object.
  * This is why there is not a <CODE>set</CODE> method for the drctDbtTxInf property.
  * 
  * <p>
  * For example, to add a new item, do as follows:
  * <pre>
  *    getDrctDbtTxInf().add(newItem);
  * </pre>
  * 
  * 
  * <p>
  * Objects of the following type(s) are allowed in the list
  * {@link Document.DrctDbtTxInf }
  * 
  * 
  */
 public List<Document.DrctDbtTxInf> getDrctDbtTxInf() {
     if (drctDbtTxInf == null) {
         drctDbtTxInf = new ArrayList<Document.DrctDbtTxInf>();
     }
     return this.drctDbtTxInf;
 }

 public void setDrctDbtTxInf(Document.DrctDbtTxInf transaction) {
     drctDbtTxInf.add(transaction);
 }

 public String getNameTransaction() {
     return nameTransaction;
 }

 public void setNameTransaction(String name) {
     nameTransaction = name;
 }

 public void setDepot(Document.DrctDbtTxInf doc) {
     drctDbtTxInf.add(doc);
 }

 public Document.Stats getStats() {
     return stats;
 }

 public Document.Resume getResume() {
     return resume;
 }

 public Document.Trx getTrx() {
     return trx;
 }


 /**
  * <p>Classe Java pour anonymous complex type.
  * 
  * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="PmtInf" type="{http://univ.fr/sepa}Max35Text"/>
  *         &lt;element name="InstdAmt" type="{http://univ.fr/sepa}Amount"/>
  *         &lt;element name="DrctDbtTx" type="{http://univ.fr/sepa}CmpsdTx"/>
  *         &lt;element name="DbtrAgt" type="{http://univ.fr/sepa}CmpsdAgt"/>
  *         &lt;element name="Dbtr" type="{http://univ.fr/sepa}CmpsdNmd"/>
  *         &lt;element name="DbtrAcct" type="{http://univ.fr/sepa}CmpsdIdt"/>
  *         &lt;element name="RmtInf" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *       &lt;/sequence>
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "pmtInf",
     "instdAmt",
     "drctDbtTx",
     "dbtrAgt",
     "dbtr",
     "dbtrAcct",
     "rmtInf"
 })
 @XmlRootElement(name = "DrctDbtTxInf")
 public static class DrctDbtTxInf {

     @XmlElement(name = "PmtInf", required = true)
     protected String pmtInf;
     @XmlElement(name = "InstdAmt", required = true)
     protected Amount instdAmt;
     @XmlElement(name = "DrctDbtTx", required = true)
     protected CmpsdTx drctDbtTx;
     @XmlElement(name = "DbtrAgt", required = true)
     protected CmpsdAgt dbtrAgt;
     @XmlElement(name = "Dbtr", required = true)
     protected CmpsdNmd dbtr;
     @XmlElement(name = "DbtrAcct", required = true)
     protected CmpsdIdt dbtrAcct;
     @XmlElement(name = "RmtInf", required = true)
     protected String rmtInf;

     /**
      * Obtient la valeur de la propriété pmtInf.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getPmtInf() {
         return pmtInf;
     }

     /**
      * Définit la valeur de la propriété pmtInf.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setPmtInf(String value) {
         this.pmtInf = value;
     }

     /**
      * Obtient la valeur de la propriété instdAmt.
      * 
      * @return
      *     possible object is
      *     {@link Amount }
      *     
      */
     public Amount getInstdAmt() {
         return instdAmt;
     }

     /**
      * Définit la valeur de la propriété instdAmt.
      * 
      * @param value
      *     allowed object is
      *     {@link Amount }
      *     
      */
     public void setInstdAmt(Amount value) {
         this.instdAmt = value;
     }

     /**
      * Obtient la valeur de la propriété drctDbtTx.
      * 
      * @return
      *     possible object is
      *     {@link CmpsdTx }
      *     
      */
     public CmpsdTx getDrctDbtTx() {
         return drctDbtTx;
     }

     /**
      * Définit la valeur de la propriété drctDbtTx.
      * 
      * @param value
      *     allowed object is
      *     {@link CmpsdTx }
      *     
      */
     public void setDrctDbtTx(CmpsdTx value) {
         this.drctDbtTx = value;
     }

     /**
      * Obtient la valeur de la propriété dbtrAgt.
      * 
      * @return
      *     possible object is
      *     {@link CmpsdAgt }
      *     
      */
     public CmpsdAgt getDbtrAgt() {
         return dbtrAgt;
     }

     /**
      * Définit la valeur de la propriété dbtrAgt.
      * 
      * @param value
      *     allowed object is
      *     {@link CmpsdAgt }
      *     
      */
     public void setDbtrAgt(CmpsdAgt value) {
         this.dbtrAgt = value;
     }

     /**
      * Obtient la valeur de la propriété dbtr.
      * 
      * @return
      *     possible object is
      *     {@link CmpsdNmd }
      *     
      */
     public CmpsdNmd getDbtr() {
         return dbtr;
     }

     /**
      * Définit la valeur de la propriété dbtr.
      * 
      * @param value
      *     allowed object is
      *     {@link CmpsdNmd }
      *     
      */
     public void setDbtr(CmpsdNmd value) {
         this.dbtr = value;
     }

     /**
      * Obtient la valeur de la propriété dbtrAcct.
      * 
      * @return
      *     possible object is
      *     {@link CmpsdIdt }
      *     
      */
     public CmpsdIdt getDbtrAcct() {
         return dbtrAcct;
     }

     /**
      * Définit la valeur de la propriété dbtrAcct.
      * 
      * @param value
      *     allowed object is
      *     {@link CmpsdIdt }
      *     
      */
     public void setDbtrAcct(CmpsdIdt value) {
         this.dbtrAcct = value;
     }

     /**
      * Obtient la valeur de la propriété rmtInf.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getRmtInf() {
         return rmtInf;
     }

     /**
      * Définit la valeur de la propriété rmtInf.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setRmtInf(String value) {
         this.rmtInf = value;
     }

 }

 @XmlRootElement(name = "stats")
 public static class Stats {

     @XmlElement(name = "nombreTransaction", required = true)
     protected int nombreTransaction;

     protected BigDecimal montant;
     @XmlElement(name = "montantTotal", required = true)
     protected int sommeTotalMontant;

     public Stats(Document doc) {
         nombreTransaction = doc.getDrctDbtTxInf().size();

         for (DrctDbtTxInf drc : doc.getDrctDbtTxInf()) {
             montant = drc.getInstdAmt().getInstdAmt().getValue();
             sommeTotalMontant += montant.intValue();
         }
     }

     public Stats() {

     }

     public int getNombreTransaction() {
         return nombreTransaction;
     }

     public int getSommeTotalMontant() {
         return sommeTotalMontant;
     }
 }

 @XmlRootElement(name = "resume")
 public static class Resume {

     @XmlElement(name = "PmtId", required = true)
     protected List<String> identifiant = new ArrayList<>();
     @XmlElement(name = "Nom", required = true)
     protected List<String> nom = new ArrayList<>();
     @XmlElement(name = "Montant", required = true)
     protected List<BigDecimal> montant = new ArrayList<>();
     @XmlElement(name = "Date", required = true)
     protected List<String> date = new ArrayList<>();

     public Resume(Document doc) {

         for (DrctDbtTxInf drc : doc.getDrctDbtTxInf()) {
             identifiant.add(drc.getPmtInf());
             nom.add(drc.getRmtInf());
             montant.add(drc.getInstdAmt().getInstdAmt().getValue());
             //date.add(drc.getDbtrAcct().getIBAN());
         }
     }

     public Resume() {

     }
 }

 @XmlRootElement(name = "Trx")
 public static class Trx {

     @XmlElement(name = "PmtId", required = true)
     protected String identifiant;
     @XmlElement(name = "Nom", required = true)
     protected String nom;
     @XmlElement(name = "Montant", required = true)
     protected BigDecimal montant;
     @XmlElement(name = "Date", required = true)
     protected String date;

     public Trx(Document doc) {

         for (DrctDbtTxInf drc : doc.getDrctDbtTxInf()) {
             if (drc.getPmtInf().equals(doc.getNameTransaction())) {
                 identifiant = drc.getPmtInf();
                 nom = drc.getRmtInf();
                 montant = drc.getInstdAmt().getInstdAmt().getValue();
                 //date = drc.getDbtrAcct().getIBAN();
             }
         }
     }

     public Trx() {

     }
 }

}
